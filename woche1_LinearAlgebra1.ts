import Playground from "./playground";

const pg = new Playground();

// Visualize grids:
pg.gridXZ();
//pg.gridXY();
//pg.gridYZ();
/*
const v = [1, 2, 3];
pg.visVector(v);
pg.visVector(v, { color: "dodgerblue" });

// Linear Algebra 1 Aufgabe 1

function vecLength(p: Array<number>) {

    const sum = (p[0] * p[0]) + (p[1] * p[1]) + (p[2] * p[2]);

    const result = Math.sqrt(sum);

    return result;

}

let result = vecLength(v);

let resultString: string = String(vecLength(v))


//  Linear Algebra 1 Aufgabe 2

function vecNormalize(p: Array<number>, Length: number) {

    if (!Length) {

        Length = 1;

    }

    const scaledX = p[0] / Length;
    const scaledY = p[1] / Length;
    const scaledZ = p[2] / Length;

    return [scaledX, scaledY, scaledZ];

}

let scaledVec = vecNormalize(v, result);
pg.visVector(v, { color: "blue", label: "the length is " + resultString, triangles: false })
pg.visVector(scaledVec, { color: "red", label: "Normalized", triangles: false })



// Linear Algebra 1 Aufgabe 3


const a = [4, 5, 6];
const b = [7, 1, 2];
pg.visVector(a);
pg.visVector(b);



function vecDotProduct(a: Array<number>, b: Array<number>) {

    const product = (a[0] * b[0]) + (a[1] * b[1]) + (a[2] * b[2]);

    return product;

}


let productString: string = String(vecDotProduct(a,b))


pg.visVector(a, { color: "black", label: "A and B has the scale " + productString});
pg.visVector(b, { color: "green", label: "B" }); */


//Linear Algebra 1 Aufgabe 4

const p = [2, 0, 0];
const q = [2, 2, 0];

pg.visVector(p, { color: "red", label: "P" });


function vecLength(vector: Array<number>) {

    const sum = (vector[0] * vector[0]) + (vector[1] * vector[1]) + (vector[2] * vector[2]);

    const result = Math.sqrt(sum);

    return result;

}

function vecDotProduct (vec1: Array<number>, vec2: Array<number>) {

    const product = (vec1[0] * vec2[0]) + (vec1[1] * vec2[1]) + (vec1[2] * vec2[2]);

    return product;

}

function calAngle(vec1: number[], vec2: number[]): number {
   
    const dotProduct = vecDotProduct(vec1, vec2);


    const lengthP = vecLength(vec1);
    const lengthQ = vecLength(vec2);

    const radian = Math.acos(dotProduct / (lengthP * lengthQ));

    return radian;
}

function radiansToDegrees(radians: number): number {

    const degrees = radians * (180 / Math.PI);
    return degrees;
}

const angleRadians = calAngle(p, q);
const angleDegrees = radiansToDegrees(angleRadians);

const angleLabel = `${angleDegrees.toFixed(0)} degrees`;


pg.visVector(q, { color: "blue", label: "Q " + angleLabel });// wie kann ich die Beschriftung zwischen 2 Vektoren platzieren?


// Cross Product Linear Algebra 1 Aufgabe 5

function vecCrossProduct (a:Array<number>, b: Array<number>) {

    const newX = (a[1] * b[2]) - (a[2] * b[1]);
    const newY = (a[0] * b[2]) - (a[2] * b[0]);
    const newZ = (a[0] * b[1]) - (a[1] * b[0]);

    return [newX, newY, newZ];


}

let crossProduct = vecCrossProduct(p, q);
pg.visVector(crossProduct, { color: "purple", label: "Cross'", triangles: false })


