console.log("Hi there!")

import Playground from "./playground";
import { perspDivide } from "./_helper";


const pg = new Playground();

 // Visualize grids:
pg.gridXZ();
//pg.gridXY();
//pg.gridYZ();

// A vector is just an array:
const v = [1, 2, 3];
const w = [2, 2, 3];

const substraction = [v[0]-w[0], v[1]-w[1], v[2]-w[2]];

// Visualize a vector
pg.visVector(v);
pg.visVector(w);
pg.visVector(substraction);

// Add optional parameters to the visualization:
pg.visVector(v, { color: "dodgerblue", label: "V" });
pg.visVector(w, { color: "red", label: "W" });
pg.visVector(substraction, { color: "green", placeAt:w });

// Visualize the same data as point:
pg.visPoint(v, { pscale: .01 });
pg.visPoint(w, { pscale: .01 }); 


// perspective Divide
const pointClose = [0.5, 0.6, -3];
const pointFar = [0.5, 0.6, -6];

const distImagePlane = -1; //distanz zwischen Bildebene und Kamera

pg.visCamera(distImagePlane);
pg.visVector(pointClose, { color: "orange", label: "C", triangles: true })
pg.visVector(pointFar, { color: "blue", label: "F", triangles: true })

//function to calculate the x' and y'
function perspDivide(p: Array<number>, dist: number) {

    if (p[2] === 0) {
        console.warn('Warnung: Division durch Null nicht möglich');
        return [0, 0];
    }

    else {    
    
    const x = (p[0] * dist) / p[2];
    const y = (p[1] * dist) / p[2];

    return [x, y, dist];

    }

}

let cProjected = perspDivide(pointClose, distImagePlane);
let fProjected = perspDivide(pointFar, distImagePlane);

pg.visVector(cProjected, { color: "orange", label: "C'", triangles: true })
pg.visVector(fProjected, { color: "blue", label: "F'", triangles: true })


// ray equation
/*
const origin = [0, 0, 0]  // das O
const m = [0.5, 0.6, -3]; // das V
const travelDist = 0.5;


pg.visPoint(origin, { color: "orange", label: "O", pscale: .02 });
pg.visVector(v, { color: "blue", label: "V" });

function substraction2 (v1: Array<number>, v2: Array<number>) {

    const result = [v2[0]-v1[0], v2[1]-v1[1], v2[2]-v1[2]];

    return result;

}

function rayEquation (origin: Array<number>, v: Array<number>, travelDist: number ) {

    const DistVO = substraction2 (origin, v)
    const p0 = DistVO[0] * travelDist + origin[0];
    const p1 = DistVO[1] * travelDist + origin[1];
    const p2 = DistVO[2] * travelDist + origin[2];

    return [p0, p1, p2];

}

let p = rayEquation(origin, v, travelDist);
pg.visVector(p, { color: "red", label: "P", triangles: false });
*/